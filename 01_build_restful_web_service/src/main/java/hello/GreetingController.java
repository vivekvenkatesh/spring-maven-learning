package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {
	private static final String message = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	
	// Request Mapping ensures that HTTP requests to /greeting are mapped
	// to the greeting() method
	// The code below does not however specify if its GET, PUT or POST

	@RequestMapping("/greeting")

	// ResponseBody annotation tells Spring MVC that id does not need to
	// render the greeting object through a server side view layer, but that instead
	// that the greeting object should be written out directly.

	// The greeting object is converted automatically to JSON (Jackson 2 is on the classpath)
	// MappingJackson2HttpMessageConverter

	public @ResponseBody Greeting greeting(
			@RequestParam (value="name", required=false, defaultValue="World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(message, name));
	}
}
