package hello;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan // Search recursively though the hello package and its children for @Component (or @Controller) 
@EnableAutoConfiguration

// Enable AutoConfiguration
// switches on reasonable default behaviors based on the content of your classpath
// Ex: because the application depends on the embeddable version of Tomcat (tomcat-embed-core.jar), 
// a Tomcat server is set up and configured with reasonable defaults on your behalf
// And because the application also depends on Spring MVC (spring-webmvc.jar), 
// a Spring MVC DispatcherServlet is configured and registered for you

public class Application {

	// This program is for making the application executable

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
